# ---------------------------------------------------------------------------------------------------------------------
#  Instance Jenkins
# ---------------------------------------------------------------------------------------------------------------------

variable "region" {
  description = "The AWS region"
  default     = "us-east-1"
}

variable "key_name" {
  description = "The name of the key_pair "   
  default     = "ec2-jenkins-server"  
}

variable "iam_name" {
  description = "The name of the policy "   
  default     = "AdminPolicy"  
}

variable "instance-ami" {
  description = "The AMI (Amazon Machine Image) that identifies the instance"
  default     = "ami-04823729c75214919"
}

variable "instance-type" {
  description = "The instance type to be used"
  default     = "t3.medium"
}

variable "instance-tag-name" {
  description = "instance-tag-name"
  default     = "ec2-jenkins-server"
}

variable "vpc-id" {
  description = "ID VPC"
  default     = "vpc-0d20fa624ffc6f196"
}

variable "subnet-id-jenkins" {
  description = "ID subnet"
  default     = "subnet-07b38e884fe1d5804"
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = ["subnet-0b14ed38858772513","subnet-0a6a94dc24b942bcb"]
}

variable "ebs-size" {
  description = "Size to ebs"
  default     = "30"
}

variable "ebs-type" {
  description = "Size to ebs"
  default     = "gp2"
}

variable "sg-pub-name" {
  default      = "jenkins-server-SG"
}


# ---------------------------------------------------------------------------------------------------------------------
#  ELB
# ---------------------------------------------------------------------------------------------------------------------

variable "security_group_alb" {
  type        = string
  default      = "security_group_alb"
} 
variable "name-alb" {
  type        = string
  default      = "ALB"
}

# ---------------------------------------------------------------------------------------------------------------------
# TARGET GROUPS
# ---------------------------------------------------------------------------------------------------------------------

variable "name-tg-jenkins" {
  type        = string
  default      = "alb-target-group-cbre-jenkins"
}

