provider "aws" {
  region = var.region
}
##########################
# IAM AND POLICY
##########################


data "aws_iam_policy_document" "admin" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}
resource "aws_iam_role" "admin" {
  assume_role_policy = data.aws_iam_policy_document.admin.json
  name               = var.iam_name
}
resource "aws_iam_role_policy_attachment" "admin" {
  role       = aws_iam_role.admin.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
resource "aws_iam_instance_profile" "admin" {
  name = var.iam_name
  role = aws_iam_role.admin.name
}


######################################
# Key pair
######################################

resource "tls_private_key" "llave" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.llave.public_key_openssh
}



######################################
# Security Group Jenkins
######################################

resource "aws_security_group" "sg" {
  name   = var.sg-pub-name
  vpc_id = var.vpc-id

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "22"
    to_port     = "22"
  }

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "8080"
    to_port     = "8080"
  }

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "80"
    to_port     = "80"
  }

  ingress {
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "443"
    to_port     = "443"
  }

  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    to_port     = "0"
  }

  tags = {
    Name = var.sg-pub-name
  }
}

######################################
# Create EC2
######################################

resource "aws_instance" "jenkins" {
  ami                         = var.instance-ami
  associate_public_ip_address = false
  iam_instance_profile        = aws_iam_instance_profile.admin.name
  instance_type               = var.instance-type
  key_name                    = aws_key_pair.generated_key.key_name
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = var.subnet-id-jenkins
  user_data = "${file("user-data.sh")}"
  root_block_device {
    volume_size                 = var.ebs-size
    volume_type                 = var.ebs-type
  }

  tags = {
    Name = var.instance-tag-name
  }
}

################
## LOAN BALANCER / LISTENERS
################
resource "aws_alb" "alb" {
  name            = var.name-alb
  security_groups = [aws_security_group.alb.id]

  subnets = var.public_subnets
  
}

resource "aws_alb_listener" "jenkins" {
  load_balancer_arn = aws_alb.alb.arn
  port              = 8080
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.jenkins.arn
    type             = "forward"
    }
}

resource "aws_lb_target_group_attachment" "jenkins" {
  target_group_arn = aws_alb_target_group.jenkins.arn
  target_id        = aws_instance.jenkins.id
  port             = 8080
}

################
## TARGET GROUPS
################

resource "aws_alb_target_group" "jenkins" {
  deregistration_delay = 30
  slow_start = 240
  health_check {
    path = "/login"
    healthy_threshold = 5
    unhealthy_threshold = 5
    interval = 30
  }

  name     = var.name-tg-jenkins
  port     = 8080
  protocol = "HTTP"

  stickiness {
    type = "lb_cookie"
  }

  vpc_id = var.vpc-id
}

################
## Security Group ALB
################

resource "aws_security_group" "alb" {

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }


  name = var.security_group_alb

  tags = {
    Env  = "Test"
    Name = var.security_group_alb
  }

  vpc_id = var.vpc-id
}

